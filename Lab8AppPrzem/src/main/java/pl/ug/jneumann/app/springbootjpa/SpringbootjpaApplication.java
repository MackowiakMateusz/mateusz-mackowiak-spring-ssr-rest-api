package pl.ug.jneumann.app.springbootjpa;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.ug.jneumann.app.springbootjpa.service.CarService;

@SpringBootApplication
public class SpringbootjpaApplication {
  //http://hsqldb.org/doc/guide/running-chapt.html ważne do uruchommienia ogarnalem sobie na zajeciach, fajne rzeczy tam mozna doczytac jeszcze

  //najpierw trzeba uruchomic to:

  //java -cp ./db/hsqldb.jar org.hsqldb.server.Server --database.0 mem:mydb --dbname.0 workdb

  // a to ponizej to tkai pocjonalny klient mozna sobie poogladac

  //java -cp ./db/hsqldb.jar org.hsqldb.util.DatabaseManagerSwing --url jdbc:hsqldb:hsql://localhost/workdb
  public static void main(String[] args) {
    SpringApplication.run(SpringbootjpaApplication.class, args);
  }

  @Bean
  public CommandLineRunner setUpApp(@Autowired CarService carService) {
    return (args) -> {

      carService.learning();

    };
  }

}
