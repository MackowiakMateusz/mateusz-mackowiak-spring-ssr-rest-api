package com.ug.edu.mmackowiak.app.starwebapi.domain;

public class Star {

  private String id;
  private String name;
  private String category;
  private int ageInEarthYears;
  //for example:
  /*
  * White Dwarf
  * Brown Dwarf
  * Red Giant
  * Giant Star
  * Neutron Star
  * Protostar
  * Supergiant
  * Red Dwarf
  * Black Hole
  * */

  public Star(String name, String category ,int ageInEarthYears) {
    this.name = name;
    this.ageInEarthYears = ageInEarthYears;
    this.category = category;
  }

  public Star(String id, String name,String category, int ageInEarthYears) {
    this.id = id;
    this.name = name;
    this.ageInEarthYears = ageInEarthYears;
    this.category = category;
  }

  public Star() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAgeInEarthYears() {
    return ageInEarthYears;
  }

  public void setAgeInEarthYears(int ageInEarthYears) {
    this.ageInEarthYears = ageInEarthYears;
  }
}
