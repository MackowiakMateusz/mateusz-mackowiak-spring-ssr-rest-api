package pl.ug.jneumann.app.springbootjpa.domain;

import javax.persistence.*;

@Entity
public class Book {

  private Long id;
  private String title;
  private int yop;

  public Book() {
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getYop() {
    return yop;
  }

  public void setYop(int yop) {
    this.yop = yop;
  }
}
