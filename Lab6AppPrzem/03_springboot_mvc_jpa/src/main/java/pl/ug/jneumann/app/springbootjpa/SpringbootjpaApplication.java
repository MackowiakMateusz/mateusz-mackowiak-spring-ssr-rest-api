package pl.ug.jneumann.app.springbootjpa;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringbootjpaApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringbootjpaApplication.class, args);
  }

  @Bean
  public CommandLineRunner setUpApp() {
    return (args) -> {


    };
  }

}
