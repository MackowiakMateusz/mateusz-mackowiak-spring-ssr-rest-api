package org.example;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.example.Compute;

public class Main {
    public static void main(String[] args) {
        System.out.println("Polish Exercise Description\n" +
                "1. Utwórz projekt za pomocą narzędzia Gradle i swojego środowiska programistycznego.\n" +
                "2. Skonfiguruj plik build.gradle tak, aby w projekcie używana była Java w wersji 11.\n" +
                "3. Następnie dodaj pakiet Apache Commons Math (wskazówka: wyszukaj pakiet w https://mvnrepository.com/)\n" +
                "4. Korzystając z tego pakietu w Twojej klasie Compute zaimplementuj metodę wyliczającą dla serii zadanych liczb typu double\n" +
                "5. wartości statystyczne: średnią arytmetyczną, odchylenie standardowe, wariancję\n" +
                "6. Spakuj ten projektu do formatu .jar, a następnie wywołaj powyższą metodę w metodzie main z poziomu konsoli.\n");

        System.out.println("Seria danych została wypisana ponizej.");
        double[] dataSeries={1.0,2.0,3.0,4.0,5.0};
        for (int i = 0; i < dataSeries.length; i++) {
            System.out.println(dataSeries[i]);
        }
        System.out.println("Seria danych została wypisana powyzej.\n");
        System.out.println("Statistic value: mean");
        System.out.println(Compute.computeMeanAndStandardDeviationAndVarianecToDoubleArray(dataSeries)[0]);
        System.out.println("Statistic value: standard deviation");
        System.out.println(Compute.computeMeanAndStandardDeviationAndVarianecToDoubleArray(dataSeries)[1]);
        System.out.println("Statistic value: variance");
        System.out.println(Compute.computeMeanAndStandardDeviationAndVarianecToDoubleArray(dataSeries)[2]);
    }
}