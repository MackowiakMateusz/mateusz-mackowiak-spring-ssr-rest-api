package org.example;

import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

public class Compute {
    public static double[] computeMeanAndStandardDeviationAndVarianecToDoubleArray(double [] dataSeries)
    {
        double meanValue=StatUtils.mean(dataSeries);
        StandardDeviation standardDeviationClass= new StandardDeviation ();
        double standardDeviationValue=standardDeviationClass.evaluate(dataSeries);
        double varianceValue=StatUtils.variance(dataSeries);
        double [] resultArray={meanValue,standardDeviationValue,varianceValue};
        return resultArray;
    }
}
