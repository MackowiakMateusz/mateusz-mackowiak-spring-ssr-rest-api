package pl.ug.jneumann.app.springbootjpa.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Book {

  private Long id;
  private String title;
  private int yop;
  private List<Person> authors;
  private List<Person> reviewer;

  public Book() {
  }

  @ManyToMany(mappedBy = "reviewed")
  public List<Person> getReviewer() {
    return reviewer;
  }

  public void setReviewer(List<Person> reviewer) {
    this.reviewer = reviewer;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @ManyToMany(mappedBy = "books")
  public List<Person> getAuthors() {
    return authors;
  }

  public void setAuthors(List<Person> authors) {
    this.authors = authors;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getYop() {
    return yop;
  }

  public void setYop(int yop) {
    this.yop = yop;
  }
}
