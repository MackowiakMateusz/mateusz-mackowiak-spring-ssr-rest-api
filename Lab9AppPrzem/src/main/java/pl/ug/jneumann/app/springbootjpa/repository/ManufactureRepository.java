package pl.ug.jneumann.app.springbootjpa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.ug.jneumann.app.springbootjpa.domain.Manufacture;

import java.util.List;

@Repository
public interface ManufactureRepository extends CrudRepository<Manufacture, Long> {

  List<Manufacture> findByCountry(String country);
  List<Manufacture> findByCountryOrYof(String country, int yof);

  // Query: Świat obiektów JPQL
  @Query("Select m from Manufacture m")
  List<Manufacture> getAllManufactures();

  // Query: Świat obiektów JPQL // zadanie 1
  @Query("Select m from Manufacture m where m.country=?1 and m.yof=?2")
  List<Manufacture> getByCountryAndYof(String country, int yof);
  //zadanie 1
  @Query(nativeQuery = true, value = "Select * from Manufacture where country=?1 or yof=?2")
  List<Manufacture> getByCountryOrYof(String country,int yof);

  // Query: Świat relacyjny SQL
  @Query(nativeQuery = true, value = "Select * from Manufacture where yof=?")
  List<Manufacture> getByYof(int yof);





}
