package pl.ug.jneumann.app.springbootjpa.domain;

import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.List;


@Entity
public class Person {

  private Long id;
  private String firstName;
  private int yob;

  private List<Book> books;
  private List<Book> reviewed;

  public Person() {
  }

  public Person(String firstName, int yob) {
    this.firstName = firstName;
    this.yob = yob;
  }

  @ManyToMany
  public List<Book> getReviewed() {
    return reviewed;
  }

  public void setReviewed(List<Book> reviewed) {
    this.reviewed = reviewed;
  }

  @ManyToMany
  public List<Book> getBooks() {
    return books;
  }

  public void setBooks(List<Book> books) {
    this.books = books;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @NonNull
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  @Column(name = "year_of_birth")
  public int getYob() {
    return yob;
  }

  public void setYob(int yob) {
    this.yob = yob;
  }
}
