package pl.ug.jneumann.app.springbootjpa.controller;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Manufacture id DB")
public class ManufactureNotFoundException extends RuntimeException {

}
