package pl.ug.jneumann.app.springbootjpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ug.jneumann.app.springbootjpa.domain.Manufacture;
import pl.ug.jneumann.app.springbootjpa.service.ManufactureManager;

import java.util.List;
@RestController("ManufactureController")
public class ManufactureController {
    private final ManufactureManager manufactureManager;
    public ManufactureController(@Autowired ManufactureManager manufactureManager) {
        this.manufactureManager = manufactureManager;
    }
    @PostMapping("/api/manufacture")
    Manufacture addManufacture(@RequestBody Manufacture manufacture) {
        return manufactureManager.addManufacture(manufacture);
    }
    @PutMapping("/api/manufacture/{id}")
    Manufacture modifyManufacture(@RequestBody Manufacture manufacture) {
        //TODO do it yourself
        return  manufactureManager.editManufacture(manufacture);
    }
    @GetMapping("/api/manufacture")
    List<Manufacture> getAll() {
        return manufactureManager.getAllManufactures();
    }
    @GetMapping("/api/manufacture/{id}")
    Manufacture findManufacture(@PathVariable("id") String id) {
        Manufacture foundManufacture = manufactureManager.findById(id);
        if (foundManufacture == null) {
            throw new ManufactureNotFoundException();
        }
        return foundManufacture;
    }
    @DeleteMapping("/api/manufacture/{id}")
    public void deleteManufacture(@PathVariable("id") String id){
        manufactureManager.deleteManufacture(id);
    }

}
