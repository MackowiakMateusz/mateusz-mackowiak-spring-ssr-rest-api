package pl.ug.jneumann.app.springbootjpa.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class  Manufacture {

  private Long id;
  private String name;
  private String country;
  private int yof;

  private List<Car> cars;

  public Manufacture(String name ,String country, int yof) {
    this.name = name;
    this.country = country;
    this.yof = yof;
  }

  public Manufacture() {
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


//  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @OneToMany/*@ManyToMany*/(cascade = CascadeType.ALL, /*fetch = FetchType.EAGER*/ fetch = FetchType.LAZY)//zadanie3
  public List<Car> getCars() {
    return cars;
  }

  public void setCars(List<Car> cars) {
    this.cars = cars;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public int getYof() {
    return yof;
  }

  public void setYof(int yof) {
    this.yof = yof;
  }

  @Override
  public String toString() {
    return "Manufacture{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", country='" + country + '\'' +
        ", yof=" + yof +
        '}';
  }
}
