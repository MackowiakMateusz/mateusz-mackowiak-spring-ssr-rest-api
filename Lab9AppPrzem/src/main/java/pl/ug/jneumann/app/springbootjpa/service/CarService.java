package pl.ug.jneumann.app.springbootjpa.service;


import org.springframework.stereotype.Service;
import pl.ug.jneumann.app.springbootjpa.domain.Car;
import pl.ug.jneumann.app.springbootjpa.domain.Manufacture;
import pl.ug.jneumann.app.springbootjpa.repository.CarRepository;
import pl.ug.jneumann.app.springbootjpa.repository.ManufactureRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CarService {
//http://hsqldb.org/doc/guide/running-chapt.html ważne do uruchommienia ogarnalem sobie na zajeciach, fajne rzeczy tam mozna doczytac jeszcze

  //najpierw trzeba uruchomic to:

  //java -cp ./db/hsqldb.jar org.hsqldb.server.Server --database.0 mem:mydb --dbname.0 workdb

  // a to ponizej to tkai pocjonalny klient mozna sobie poogladac

  //java -cp ./db/hsqldb.jar org.hsqldb.util.DatabaseManagerSwing --url jdbc:hsqldb:hsql://localhost/workdb
  final CarRepository carRepository;
  final ManufactureRepository manufactureRepository;

  public CarService(CarRepository carRepository, ManufactureRepository manufactureRepository) {
    this.carRepository = carRepository;
    this.manufactureRepository = manufactureRepository;
  }

  public Car addCar(Car car) {
    return carRepository.save(car);
  }

  public Iterable<Manufacture> findAllManufactures(){
    return manufactureRepository.findAll();
  }

  public void learning() {
    // Tx.begin
    Car car1 = new Car("Fiat", "Bravo", 1990);
    Car car2 = new Car("Fiat", "Brava", 1990);
    Car car3 = new Car("Ford", "Mondeo", 1990);

    List<Car> cars = new ArrayList<>();
    cars.add(car1);
    cars.add(car2);
    cars.add(car3);
    //carRepository.saveAll(cars);

    Manufacture fiatManufacture = new Manufacture("Fabbrica Italiana Automobili Torino","Italy",1925);

    fiatManufacture.setCars(cars);
    //fiatManufacture.setCountry("Italy");
    //fiatManufacture.setYof(1925);

    Manufacture manufactureRetrieved = manufactureRepository.save(fiatManufacture);

    Optional<Manufacture> manufactureOpt = manufactureRepository.findById(manufactureRetrieved.getId());

    manufactureOpt.ifPresent(manufacture -> {
      System.out.println(manufacture.getCars().size());
    });

    Manufacture fordManufacture = new Manufacture("Henry Ford car manufacture","USA",1926);

    //fordManufacture.setCountry("USA");
    //fordManufacture.setYof(1926);

    manufactureRepository.save(fordManufacture);

    manufactureRepository.findByCountry("USA").forEach(System.out::println);
    manufactureRepository.getByCountryOrYof("USA", 1925).forEach(System.out::println);//zadanie 1
    manufactureRepository.getByCountryAndYof("USA", 1926).forEach(System.out::println);//zadanie 1
    fordManufacture.setCountry("United States of America");


    // Tx.commit
    // Tx.rollback
  }

}
