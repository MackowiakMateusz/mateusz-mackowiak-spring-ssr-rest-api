package pl.ug.jneumann.app.springbootjpa.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.ug.jneumann.app.springbootjpa.domain.Manufacture;
import pl.ug.jneumann.app.springbootjpa.service.CarService;

import java.util.List;

@RestController
public class ManufactureController {

  final
  CarService carService;

  public ManufactureController(CarService carService) {
    this.carService = carService;
  }

  @GetMapping("/api/manufacture")
  Iterable<Manufacture> allManufactures(){
    return carService.findAllManufactures();
  }
}
