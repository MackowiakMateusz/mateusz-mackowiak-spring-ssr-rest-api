package pl.ug.jneumann.app.springbootjpa.service;

import pl.ug.jneumann.app.springbootjpa.domain.Manufacture;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class ManufactureManagerInMemoryImpl implements ManufactureManager{

  // STUB ONLY

  // Map<String, Manufacture> db ?
  List<Manufacture> db = Collections.synchronizedList(new ArrayList<>());

  private Manufacture manufacture;

  @Override
  public Manufacture addManufacture(Manufacture manufacture) {
    Manufacture manufactureToAdd = new Manufacture(manufacture.getName(), manufacture.getCountry(), manufacture.getYof());
    db.add(manufactureToAdd);
    return manufactureToAdd;
  }

  @Override
  public Manufacture editManufacture(Manufacture manufactureToEdit) {
    Manufacture manufactureToRemove = null;
    for (Manufacture manufacture : db) {
      if (manufactureToEdit.getId().equals(manufacture.getId())) {
        manufactureToRemove = manufacture;
      }
    }

    if (manufactureToRemove != null) {
      db.remove(manufactureToRemove);
      db.add(manufactureToEdit);
      return manufactureToEdit;
    }
    return null;

  }

  @Override
  public Manufacture findById(String id){
    Manufacture manufactureToFind = null;

    for (Manufacture manufacture : db) {
      if (manufacture.getId().equals(id)) {
        manufactureToFind = manufacture;
      }
    }

    return manufactureToFind;
  }

  @Override
  public List<Manufacture> getAllManufactures() {
    return db;
  }

  @Override
  public boolean deleteManufacture(String id) {

    Manufacture manufactureToDelete = null;
    for (Manufacture manufacture : db) {
      if (id.equals(manufacture.getId())) {
        manufactureToDelete = manufacture;
      }
    }

    if (manufactureToDelete != null) {
      db.remove(manufactureToDelete);
      return true;
    }
    return false;
  }
}
