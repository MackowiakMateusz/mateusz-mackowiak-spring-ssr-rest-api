package pl.ug.jneumann.app.springbootjpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.ug.jneumann.app.springbootjpa.domain.Car;
import pl.ug.jneumann.app.springbootjpa.repository.CarRepository;
import pl.ug.jneumann.app.springbootjpa.service.CarService;

import java.util.Optional;

@SpringBootApplication
public class SpringbootjpaApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringbootjpaApplication.class, args);
  }

  @Bean
  public CommandLineRunner setUpApp(CarService carService) {
    return (args) -> {


      carService.learning();

//			Optional<Car> foundCar = carRepository.findById(retrievedCar.getId());
//
//			foundCar.ifPresent(System.out::println);

    };
  }

}
