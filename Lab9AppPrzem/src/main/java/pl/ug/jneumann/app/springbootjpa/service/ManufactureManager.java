package pl.ug.jneumann.app.springbootjpa.service;

import pl.ug.jneumann.app.springbootjpa.domain.Manufacture;

import java.util.List;

public interface ManufactureManager {

  Manufacture addManufacture(Manufacture manufacture);
  Manufacture editManufacture(Manufacture manufacture);
  List<Manufacture> getAllManufactures();
  boolean deleteManufacture(String id);
  Manufacture findById(String id);


}
