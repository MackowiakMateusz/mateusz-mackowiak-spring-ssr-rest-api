package com.ug.edu.mmackowiak.app.starwebapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import com.ug.edu.mmackowiak.app.starwebapi.domain.Star;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class StarCreator {

  static List<Star> starToAdd = new ArrayList<>();
  static int counter = -1;
  static {
    starToAdd.add(new Star("C12","Red Giant", 3));
    starToAdd.add(new Star("Alpha Centauri","Yellow Giant (G2)", 2));
    starToAdd.add(new Star("Whatever Star","Whatever Star Category", 4));
    starToAdd.add(new Star("Boombastic","Black Hole", 1));
  }

  @Bean
  @Scope("prototype")
  Star createBean() {
    counter++;
    return starToAdd.get(counter);
  }
}
