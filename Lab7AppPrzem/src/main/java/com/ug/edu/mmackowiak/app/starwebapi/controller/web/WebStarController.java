package com.ug.edu.mmackowiak.app.starwebapi.controller.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.ug.edu.mmackowiak.app.starwebapi.domain.Star;
import com.ug.edu.mmackowiak.app.starwebapi.service.StarManager;

@Controller
public class WebStarController {

  private final StarManager starManager;

  public WebStarController(@Autowired StarManager starManager) {
    this.starManager = starManager;
  }

  @GetMapping("/star")
  public String stars(Model model) {
    model.addAttribute("allStarsFromDB", starManager.getAllStars());

    // Kolejny widok do reenderowania, identifkator logiczny widoku do renderowania
    return "star-all";
  }

  @PostMapping("/star/addForm")
  public String addNewStar(@ModelAttribute Star starToAdd, Model model) {
    starManager.addStar(starToAdd);
    model.addAttribute("allStarsFromDB", starManager.getAllStars());
    return "star-all";
  }

  @PutMapping("/star/editForm/{id}")
  public String editStar(@PathVariable("id") String id,@ModelAttribute Star starToEdit, Model model) {
    starManager.editStar(starToEdit,id);
    model.addAttribute("allStarsFromDB", starManager.getAllStars());
    return "star-all";
  }

  @GetMapping("/star/addForm")
  public String starAdd(Model model) {
    model.addAttribute("starToAdd", new Star("unknown","unknown", 0));
    // Kolejny widok do reenderowania, identifkator logiczny widoku do renderowania
    return "star-add";
  }

  @GetMapping("/star/editForm/{id}")
  public String starEdit(@PathVariable("id") String id,Model model) {
    Star star = starManager.findById(id);
    model.addAttribute("starToEdit", new Star(star.getName(),star.getCategory(), star.getAgeInEarthYears()));
    // Kolejny widok do reenderowania, identifkator logiczny widoku do renderowania
    return "star-edit";
  }



  @GetMapping("/star/delete/{id}")
  public String deleteStar(@PathVariable("id") String id, Model model) {
    if (starManager.deleteStar(id)) {
      model.addAttribute("successMessage", "Operacja się powiodła");
    //  return "success";
    }
    else {
      model.addAttribute("errorMessage", "Operacja się nie powiodła");
//    return "error";
    }
    return "redirect:/star";
  }


}
