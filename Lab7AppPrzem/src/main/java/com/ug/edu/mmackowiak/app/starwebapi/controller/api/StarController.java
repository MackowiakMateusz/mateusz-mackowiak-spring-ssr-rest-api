package com.ug.edu.mmackowiak.app.starwebapi.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ug.edu.mmackowiak.app.starwebapi.domain.Star;
import com.ug.edu.mmackowiak.app.starwebapi.service.StarManager;

import java.util.List;

@RestController
public class StarController {

  private final StarManager starManager;

  public StarController(@Autowired StarManager starManager) {
    this.starManager = starManager;
  }

  @PostMapping("/api/star")
  Star addStar(@RequestBody Star star) {
    return starManager.addStar(star);
  }

  @PutMapping("/api/star/{id}")
  Star modifyStar(@RequestBody Star star) {
    //TODO do it yourself
    return  starManager.editStar(star);
  }

  @GetMapping("/api/star")
  List<Star> getAll() {
    return starManager.getAllStars();
  }

  @GetMapping("/api/star/{id}")
  Star findStar(@PathVariable("id") String id) {
    Star foundStar = starManager.findById(id);
    if (foundStar == null) {
       throw new StarNotFoundException();
     }

    return foundStar;
  }

  @DeleteMapping("/api/star/{id}")
  public void deleteStar(@PathVariable("id") String id){
    starManager.deleteStar(id);
  }


}
