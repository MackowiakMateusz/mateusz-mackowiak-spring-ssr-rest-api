package com.ug.edu.mmackowiak.app.starwebapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import com.ug.edu.mmackowiak.app.starwebapi.domain.Star;
import com.ug.edu.mmackowiak.app.starwebapi.service.StarManager;
@SpringBootApplication
//@ImportResource("classpath:beans.xml")
public class StarWebApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarWebApiApplication.class, args);
	}

	@Bean
	public CommandLineRunner appSetup(@Autowired StarManager starManager){
		return args -> {
			System.out.println("CommandLineRunner started...");
			starManager.addStar(new Star("C12","Red Giant", 3));
			starManager.addStar(new Star("Alpha Centauri","Yellow Giant (G2)", 2));
			starManager.addStar(new Star("Whatever Star","Whatever Star Category", 4));
			starManager.addStar(new Star("Boombastic","Black Hole", 1));
		};

	}
}
