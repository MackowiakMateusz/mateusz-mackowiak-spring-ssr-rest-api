package com.ug.edu.mmackowiak.app.starwebapi.service;

import com.ug.edu.mmackowiak.app.starwebapi.domain.Star;

import java.util.List;

public interface StarManager {

  Star addStar(Star star);
  List<Star> getAllStars();
  boolean deleteStar(String id);
  Star findById(String id);
}
