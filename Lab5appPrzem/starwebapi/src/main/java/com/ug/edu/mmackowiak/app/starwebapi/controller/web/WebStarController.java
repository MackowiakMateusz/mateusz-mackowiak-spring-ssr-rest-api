package com.ug.edu.mmackowiak.app.starwebapi.controller.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import com.ug.edu.mmackowiak.app.starwebapi.domain.Star;
import com.ug.edu.mmackowiak.app.starwebapi.service.StarManager;

@Controller
public class WebStarController {

  private final StarManager starManager;

  public WebStarController(@Autowired StarManager starManager) {
    this.starManager = starManager;
  }

  @GetMapping("/star")
  public String stars(Model model) {
    model.addAttribute("allStarsFromDB", starManager.getAllStars());

    // Kolejny widok do reenderowania, identifkator logiczny widoku do renderowania
    return "star-all";
  }

  @GetMapping("/star/form")
  public String starEdit(Model model) {
    model.addAttribute("starToAdd", new Star("unknown","unknown", 0));
    // Kolejny widok do reenderowania, identifkator logiczny widoku do renderowania
    return "star-edit";
  }

  @PostMapping("/star")
  public String addNewStar(@ModelAttribute Star starToAdd, Model model) {
    starManager.addStar(starToAdd);
    model.addAttribute("allStarsFromDB", starManager.getAllStars());
    return "star-all";
  }

  @GetMapping("/star/delete/{id}")
  public String deleteStar(@PathVariable("id") String id, Model model) {
    if (starManager.deleteStar(id)) {
      model.addAttribute("successMessage", "Operacja się powiodła");
    //  return "success";
    }
    else {
      model.addAttribute("errorMessage", "Operacja się nie powiodła");
//    return "error";
    }
    return "redirect:/star";
  }


}
