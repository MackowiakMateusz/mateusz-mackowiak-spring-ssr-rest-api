package pl.ug.jneumann.app.springbootjpa.service;


import org.springframework.stereotype.Service;
import pl.ug.jneumann.app.springbootjpa.domain.Comment;
import pl.ug.jneumann.app.springbootjpa.domain.Post;
import pl.ug.jneumann.app.springbootjpa.repository.CommentRepository;
import pl.ug.jneumann.app.springbootjpa.repository.PostRepository;

import javax.transaction.Transactional;

@Service
@Transactional
public class PostService {

  final CommentRepository commentRepository;
  final PostRepository postRepository;


  public PostService(CommentRepository commentRepository, PostRepository postRepository) {
    this.commentRepository = commentRepository;
    this.postRepository = postRepository;
  }

  private void fillData() {
    Post post1 = new Post("Post 1");
    Post post2 = new Post("Post 2");
    Post post3 = new Post("Post 3");

    postRepository.save(post1);
    postRepository.save(post2);
    postRepository.save(post3);

    Comment comment1 = new Comment("Comment 1");
    Comment comment2 = new Comment("Comment 2");
    Comment comment3 = new Comment("Comment 3");
    Comment comment4 = new Comment("Comment 4");
    Comment comment5 = new Comment("Comment 5");
    Comment comment6 = new Comment("Comment 6");
    Comment comment7 = new Comment("Comment 7");
    Comment comment8 = new Comment("Comment 8");

    comment1.setPost(post1);
    comment2.setPost(post1);
    comment3.setPost(post1);

    comment4.setPost(post2);
    comment5.setPost(post2);
    comment6.setPost(post2);

    comment7.setPost(post3);
    comment8.setPost(post3);

    commentRepository.save(comment1);
    commentRepository.save(comment2);
    commentRepository.save(comment3);
    commentRepository.save(comment4);
    commentRepository.save(comment5);
    commentRepository.save(comment6);
    commentRepository.save(comment7);
    commentRepository.save(comment8);
  }

  public void learning() {
    fillData();


    // Obserwować liczbę zapytań przy klasycznej i przy własnej implementacji findAll() zrobione
    for (Comment comment : commentRepository.findAll()) {

      System.out.println(comment.getPost().getTitle());
      System.out.println(comment.getText());
    }
//Wykonać eksperymenty (na podstawie przykładu z wykładu) we własnej dziedzinie poprzez zliczanie liczby zapytań w przypadku 'N+1 queries problem' przy domyślnej implementacji findAll i przy własnej, wykorzystującej join fetch. zrobione
    for (Comment comment : commentRepository.findAll()) {

      System.out.println(comment.getPost().getTitle());
      System.out.println(comment.getText());
    }


  }
}
