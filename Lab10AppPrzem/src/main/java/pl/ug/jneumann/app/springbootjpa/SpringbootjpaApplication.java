package pl.ug.jneumann.app.springbootjpa;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import pl.ug.jneumann.app.springbootjpa.domain.Person;
import pl.ug.jneumann.app.springbootjpa.repository.PersonRepository;
import pl.ug.jneumann.app.springbootjpa.service.PostService;

import java.util.Optional;

@SpringBootApplication
public class SpringbootjpaApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringbootjpaApplication.class, args);
  }

  @Bean
  public CommandLineRunner setUpApp(PostService postService) {
    return (args) -> {
      System.out.println("dzialam");
      //postService.learning();



      PersonRepository personRepository = new PersonRepository();

      Person person = personRepository.findById(2L);
      if (person != null) {
        System.out.println(person.getName());
        System.out.println("Person found");
      }
      else {
        System.out.println("Person not found");
      }

      // Możliwe choć nie do końca zgodne z intencjami twórców wykorzystanie typu Optional
      Optional<Person> optPerson = personRepository.getById(2L);
      if (optPerson.isPresent()) {
        System.out.println(optPerson.get().getName());
      }
      else {
        System.out.println("Person not found");
      }

      // Prawidłowe, przykładowe wykorzystanie typu Optional
      optPerson.ifPresent(person1 -> System.out.println(person1.getName()));
      optPerson.ifPresent(System.out::println);

      String name = optPerson.map(Person::getName).orElse("Unknown");
      Person person3 = personRepository.getById(2L).orElseGet(() -> new Person("unknown", 1L));
      System.out.println(person3.getName());

      Person person2 = optPerson.orElseThrow(IllegalArgumentException::new);

      // Gdy kod który będzie wykorzystywał person spodziewa się być może null
      Person person4 = optPerson.orElse(null);

      //jeden z moich eksperymentow
      Optional<Person> optPersonDwa = personRepository.getByName("Lolek");
      if (optPersonDwa.isPresent()) {
        System.out.println(optPersonDwa.get().getName());
      }
      else {
        System.out.println("Person not found");
      }//dziala


    };
  }

}
