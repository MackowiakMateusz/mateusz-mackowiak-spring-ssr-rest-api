package pl.ug.jneumann.app.springbootjpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.ug.jneumann.app.springbootjpa.domain.Person;
import pl.ug.jneumann.app.springbootjpa.domain.Post;

import java.util.Optional;

@Repository
public interface PostRepository extends CrudRepository<Post, Long> {

}
