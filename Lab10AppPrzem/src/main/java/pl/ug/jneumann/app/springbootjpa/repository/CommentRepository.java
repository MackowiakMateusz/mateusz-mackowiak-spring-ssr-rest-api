package pl.ug.jneumann.app.springbootjpa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.ug.jneumann.app.springbootjpa.domain.Comment;

import java.util.List;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {

  @Query("Select c from Comment c JOIN FETCH c.post")
  List<Comment> findAll();
  @Query("SELECT c FROM Comment c JOIN FETCH c.post posts")  //Wykonać eksperymenty (na podstawie przykładu z wykładu) we własnej dziedzinie poprzez zliczanie liczby zapytań w przypadku 'N+1 queries problem' przy domyślnej implementacji findAll i przy własnej, wykorzystującej join fetch. zrobione
  List<Comment> retrieveAll();
}
