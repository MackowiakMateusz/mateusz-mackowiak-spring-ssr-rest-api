package pl.ug.jneumann.app.springbootjpa.domain;

public class Person {

  private String name;

  private Long id;

  public Person(String name, Long id) {
    this.name = name;
    this.id = id;
  }

  public Person(String name) {
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Person() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
