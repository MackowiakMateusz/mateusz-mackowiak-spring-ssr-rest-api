package pl.ug.jneumann.app.springbootjpa.repository;

import pl.ug.jneumann.app.springbootjpa.domain.Person;

import java.util.Optional;

public class PersonRepository {

  public Person findById(Long id) {

    if (id == 2L) {
      return new Person("Lolek", 2L);
    }

    return null;
  }

  public Optional<Person> getById(Long id) {

    if (id == 2L) {
      return Optional.of(new Person("Lolek", 2L));
    }

    return Optional.empty();
  }
  // Przeanalizować i zapamiętac (możliwie wiele!) przykładów użycia typu: Optional. Wykonać własne zapytania zwracające `Otpional i eksperymenty w serwisach i/lub kontrolerach. moj wlasny przklad
  public Person findByName(String name) {

    if (name == "Lolek") {
      return new Person("Lolek", 2L);
    }

    return null;
  }

  public Optional<Person> getByName(String name) {

    if (name == "Lolek") {
      return Optional.of(new Person("Lolek", 2L));
    }

    return Optional.empty();
  }
}
