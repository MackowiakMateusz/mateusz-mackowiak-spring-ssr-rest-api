package pl.ug.jneumann.app.springbootjpa.domain;

import javax.persistence.*;

@Entity
public class Comment {

  private Long id;

  private String text;

  private Post post;

  public Comment(String text) {
    this.text = text;
  }

  public Comment() {
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  @ManyToOne
  public Post getPost() {
    return post;
  }

  public void setPost(Post post) {
    this.post = post;
  }

  @Override
  public String toString() {
    return "Comment{" +
        "id=" + id +
        ", text='" + text + '\'' +
        ", post=" + post +
        '}';
  }
}
