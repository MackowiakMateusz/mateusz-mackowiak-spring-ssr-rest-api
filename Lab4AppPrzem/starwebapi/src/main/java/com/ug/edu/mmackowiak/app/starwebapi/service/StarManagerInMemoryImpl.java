package com.ug.edu.mmackowiak.app.starwebapi.service;

import org.springframework.stereotype.Service;
import com.ug.edu.mmackowiak.app.starwebapi.domain.Star;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class StarManagerInMemoryImpl implements StarManager{

  // STUB ONLY

  // Map<String, Star> db ?
  List<Star> db = Collections.synchronizedList(new ArrayList<>());

  private Star director;

  @Override
  public Star addStar(Star star) {
    Star starToAdd = new Star(UUID.randomUUID().toString(), star.getName(), star.getCategory(), star.getAgeInEarthYears());
    db.add(starToAdd);
    return starToAdd;
  }

  @Override
  public Star findById(String id){
    Star starToFind = null;

    for (Star star : db) {
      if (star.getId().equals(id)) {
        starToFind = star;
      }
    }

    return starToFind;
  }

  @Override
  public List<Star> getAllStars() {
    return db;
  }

  @Override
  public boolean deleteStar(String id) {

    Star starToDelete = null;
    for (Star star : db) {
      if (id.equals(star.getId())) {
        starToDelete = star;
      }
    }

    if (starToDelete != null) {
      db.remove(starToDelete);
      return true;
    }
    return false;
  }
}
