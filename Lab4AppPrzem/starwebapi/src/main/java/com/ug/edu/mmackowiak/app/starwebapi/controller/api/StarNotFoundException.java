package com.ug.edu.mmackowiak.app.starwebapi.controller.api;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Star id DB")
public class StarNotFoundException extends RuntimeException {

}
