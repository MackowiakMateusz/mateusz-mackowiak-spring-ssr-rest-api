package pl.ug.jneumann.jee.mvcdemo.service;

import org.springframework.stereotype.Service;
import pl.ug.jneumann.jee.mvcdemo.domain.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class PersonManagerInMemoryImpl implements PersonManager{

  // STUB ONLY

  // Map<String, Person> db ?
  List<Person> db = Collections.synchronizedList(new ArrayList<>());

  private Person director;

  @Override
  public Person addPerson(Person person) {
    Person personToAdd = new Person(UUID.randomUUID().toString(), person.getFirstName(), person.getYob());
    db.add(personToAdd);
    return personToAdd;
  }

  @Override
  public Person findById(String id){
    Person personToFind = null;

    for (Person person : db) {
      if (person.getId().equals(id)) {
        personToFind = person;
      }
    }

    return personToFind;
  }

  @Override
  public List<Person> getAllPersons() {
    return db;
  }

  @Override
  public boolean deletePerson(String id) {

    Person personToDelete = null;
    for (Person person : db) {
      if (id.equals(person.getId())) {
        personToDelete = person;
      }
    }

    if (personToDelete != null) {
      db.remove(personToDelete);
      return true;
    }
    return false;
  }
}
