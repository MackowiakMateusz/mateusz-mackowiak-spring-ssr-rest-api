package pl.ug.jneumann.jee.mvcdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import pl.ug.jneumann.jee.mvcdemo.domain.Person;
import pl.ug.jneumann.jee.mvcdemo.service.PersonManager;

@SpringBootApplication
@ImportResource("classpath:beans.xml")
public class MvcdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvcdemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner appSetup(@Autowired PersonManager personManager){
		return args -> {
			System.out.println("CommandLineRunner started...");
			personManager.addPerson(new Person("Bolek", 1967));
			personManager.addPerson(new Person("Lolek", 1937));
			personManager.addPerson(new Person("Tola", 1987));
		};

	}

}
