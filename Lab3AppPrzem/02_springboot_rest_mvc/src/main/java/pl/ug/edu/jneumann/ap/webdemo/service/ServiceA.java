package pl.ug.jneumann.jee.mvcdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.ug.jneumann.jee.mvcdemo.domain.Person;

@Service
public class ServiceA {

  private Person person;

  public ServiceA(@Qualifier("puchatek") Person person) {
    this.person = person;
  }

  public String sayHello(){
    return "Hi I'm " + person.getFirstName();
  }
}
