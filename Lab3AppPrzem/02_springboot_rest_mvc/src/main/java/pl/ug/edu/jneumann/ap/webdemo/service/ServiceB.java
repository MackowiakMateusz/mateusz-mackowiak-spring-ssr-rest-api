package pl.ug.jneumann.jee.mvcdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceB {

  private ServiceA serviceA;

  public ServiceB(ServiceA serviceA) {
    this.serviceA = serviceA;
  }

  public String hello(){
    return serviceA.sayHello();
  }

}
