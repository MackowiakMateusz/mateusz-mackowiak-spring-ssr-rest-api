package pl.ug.jneumann.jee.mvcdemo.service;

import pl.ug.jneumann.jee.mvcdemo.domain.Person;

import java.util.List;

public interface PersonManager {

  Person addPerson(Person person);
  List<Person> getAllPersons();
  boolean deletePerson(String id);
  Person findById(String id);
}
