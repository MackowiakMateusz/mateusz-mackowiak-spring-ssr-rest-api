package pl.ug.jneumann.jee.mvcdemo.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ug.jneumann.jee.mvcdemo.domain.Person;
import pl.ug.jneumann.jee.mvcdemo.service.PersonManager;
import pl.ug.jneumann.jee.mvcdemo.service.ServiceB;

import java.util.List;

@RestController
public class PersonController {

  private final PersonManager personManager;

  private final ServiceB serviceB;

  public PersonController(@Autowired PersonManager personManager, ServiceB serviceB) {
    this.personManager = personManager;
    this.serviceB = serviceB;
  }

  @PostMapping("/api/person")
  Person addPerson(@RequestBody Person person) {
    return personManager.addPerson(person);
  }

  @PutMapping("/api/person")
  Person modifyPerson(@RequestBody Person person) {
    //TODO do it yourself
    return person;
  }

  @GetMapping("/api/person")
  List<Person> getAll() {
    System.out.println(serviceB.hello());
    return personManager.getAllPersons();
  }

  @GetMapping("/api/person/{id}")
  Person findPerson(@PathVariable("id") String id) {
    Person foundPerson = personManager.findById(id);
    if (foundPerson == null) {
       throw new PersonNotFoundException();
     }

    return foundPerson;
  }

  @DeleteMapping("/api/person/{id}")
  public void deletePerson(@PathVariable("id") String id){
    personManager.deletePerson(id);
  }


}
