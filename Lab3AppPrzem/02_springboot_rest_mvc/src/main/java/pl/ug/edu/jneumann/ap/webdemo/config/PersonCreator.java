package pl.ug.jneumann.jee.mvcdemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import pl.ug.jneumann.jee.mvcdemo.domain.Person;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class PersonCreator {

  static List<Person> personToAdd = new ArrayList<>();
  static int counter = -1;
  static {
    personToAdd.add(new Person("Bolek", 1956));
    personToAdd.add(new Person("Lolek", 1956));
    personToAdd.add(new Person("Tola", 1956));
    personToAdd.add(new Person("Reksio", 1956));
  }

  @Bean
  @Scope("prototype")
  Person createBean() {
    counter++;
    return personToAdd.get(counter);
  }
}
