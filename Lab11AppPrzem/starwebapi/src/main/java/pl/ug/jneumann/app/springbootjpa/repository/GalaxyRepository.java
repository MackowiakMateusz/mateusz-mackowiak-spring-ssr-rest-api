package pl.ug.jneumann.app.springbootjpa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.ug.jneumann.app.springbootjpa.domain.Galaxy;

import java.util.List;

@Repository
public interface GalaxyRepository extends CrudRepository<Galaxy, Long> {
    @Query("Select g from Galaxy g where g.name=?1")
    List<Galaxy> findByName(String name);
}
