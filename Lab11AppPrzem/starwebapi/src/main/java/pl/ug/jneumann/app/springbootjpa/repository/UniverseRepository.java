package pl.ug.jneumann.app.springbootjpa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.ug.jneumann.app.springbootjpa.domain.Universe;

import java.util.List;

@Repository
public interface UniverseRepository extends CrudRepository<Universe, Long> {
    @Query("Select u from Universe u where u.name=?1")
    List<Universe> findByName(String name);
}
