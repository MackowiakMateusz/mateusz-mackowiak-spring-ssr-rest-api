package pl.ug.jneumann.app.springbootjpa.domain;

import org.springframework.lang.NonNull;
import pl.ug.jneumann.app.springbootjpa.domain.Star.Star;

import javax.persistence.*;
import java.util.List;


@Entity
public class Constellation {

  private Long id;
  private String name;
  private int ageInYears;
  private List<Star> owned;

  public Constellation() {
  }

  public Constellation(String name, int ageInYears) {
    this.name = name;
    this.ageInYears = ageInYears;
  }

  @ManyToMany
  public List<Star> getOwned() {
    return owned;//dodaje gwiazde do konstelacji, wiele konstelacji moze miec wiele gwiazd, kilka konstelacji moze miec wspolne dlatego many to many
  }

  public void setOwned(List<Star> owned) {
    this.owned = owned;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @NonNull
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Column(name = "age_in_years")
  public int getYob() {
    return ageInYears;
  }

  public void setYob(int ageInYears) {
    this.ageInYears = ageInYears;
  }
}
