package pl.ug.jneumann.app.springbootjpa.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.ug.jneumann.app.springbootjpa.domain.Galaxy;
import pl.ug.jneumann.app.springbootjpa.service.GalaxyService;

@RestController
public class GalaxyController {

  final
  GalaxyService galaxyService;

  public GalaxyController(GalaxyService galaxyService) {
    this.galaxyService = galaxyService;
  }

  @GetMapping("/api/galaxies")
  Iterable<Galaxy> allStars() {
    return galaxyService.findAllGalaxies();
  }

  @GetMapping("/api/galaxies/byName/{name}")
  Iterable<Galaxy> getGalaxyByName(@PathVariable String name) {
    return galaxyService.findGalaxyByName(name);
  }
}
