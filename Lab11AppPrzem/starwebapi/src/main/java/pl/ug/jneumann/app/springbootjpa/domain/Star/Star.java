package pl.ug.jneumann.app.springbootjpa.domain.Star;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Star {

  private Long id;
  private String name;
  private STARTYPE starType;
  private String lifeStage;
  private String rarity;
  private int ageInYears;

  public Star(String name, STARTYPE starType, String lifeStage, String rarity, int ageInYears) {
    this.name = name;
    this.starType = starType;
    this.lifeStage = lifeStage;
    this.rarity = rarity;
    this.ageInYears = ageInYears;
  }

  public Star() {
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public STARTYPE getColor() {
    return starType;
  }

  public void setColor(STARTYPE starType) {
    this.starType = starType;
  }

  public String getEdition() {
    return lifeStage;
  }

  public void setEdition(String lifeStage) {
    this.lifeStage = lifeStage;
  }

    public String getRarity() {
    return rarity;
  }

  public void setRarity(String rarity) {
    this.rarity = rarity;
  }

  public int getPriceInEuro() {
    return ageInYears;
  }

  public void setPriceInEuro(int ageInYears) {
    this.ageInYears = ageInYears;
  }

  @Override
  public String toString() {
    return "Star{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", starType='" + starType.starTypeName + '\'' +
        ", lifeStage='" + lifeStage + '\'' +
        ", rarity='" + rarity + '\'' +
        ", ageInYears='" + ageInYears + '\'' +
        '}';
  }
}
