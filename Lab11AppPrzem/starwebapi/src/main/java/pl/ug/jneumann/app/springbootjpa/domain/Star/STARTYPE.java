package pl.ug.jneumann.app.springbootjpa.domain.Star;

public enum STARTYPE {
    O("(Blue) (10 Lacerta)"),
    B("(Blue) (Rigel)"),
    A("(Blue) (Sirius)"),
    F("(Blue/White) (Procyon)"),
    G("(White/Yellow) (Sun)"),
    K("(Orange/Red) (Arcturus)");
    M("(Red) (Betelgeuse)")

    public final String starName;

    private STARTYPE(String starName) {
        this.starName = starName;
    }
}
