package pl.ug.jneumann.app.springbootjpa.api;

import org.jetbrains.annotations.NotNull;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.ug.jneumann.app.springbootjpa.domain.Star.STARTYPE;
import pl.ug.jneumann.app.springbootjpa.domain.Star.Star;
import pl.ug.jneumann.app.springbootjpa.service.StarService;

import java.util.Optional;

@RestController
public class StarController {

  final StarService starService;

  public StarController(StarService starService) {
    this.starService = starService;
  }

  @GetMapping("/api/stars")
  Iterable<Star> allStars() {
    return starService.findAllStars();
  }

  @GetMapping("/api/stars/{id}")
  Optional<Star> allStars(@PathVariable Long id) {
    return starService.getStarById(id);
  }

  @GetMapping("/api/stars/byStarType/{starType}")
  Iterable<Star> getStarsByStarType(@PathVariable @NotNull String starType) {
    return starService.findStarsByStarType(STARTYPE.valueOf(starType.toUpperCase()));
  }

  @GetMapping("/api/stars/byName/{name}")
  Iterable<Star> getStarsByName(@PathVariable String name) {
    return starService.findStarsByName(name);
  }

  @GetMapping("/api/stars/byStarTypeAndLifeStage/{starType}/{lifeStage}")
  Iterable<Star> getStarsByStarTypeAndLifeStage(@PathVariable @NotNull String starType, @PathVariable String lifeStage) {
    return starService.findStarsByStarTypeAndLifeStage(STARTYPE.valueOf(starType.toUpperCase()), lifeStage);
  }

  @GetMapping("/api/stars/byNameOrLifeStage/{name}/{lifeStage}")
  Iterable<Star> getStarsByNameOrLifeStage(@PathVariable String name, @PathVariable String lifeStage) {
    return starService.findStarsByNameOrLifeStage(name, lifeStage);
  }

  @GetMapping("/api/stars/byNameOrStarType/{name}/{starType}")
  Iterable<Star> getStarsByNameOrStarType(@PathVariable String name, @PathVariable @NotNull String starType) {
    return starService.findStarsByNameOrStarType(name, STARTYPE.valueOf(starType.toUpperCase()));
  }

  @DeleteMapping("/api/stars/{id}")
  void deleteStar(@PathVariable Long id) {
    starService.deleteStar(id);
  }

  @PostMapping("/api/stars")
  Star addStar(@Validated Star star) {
    return starService.addStar(star);
  }
}
