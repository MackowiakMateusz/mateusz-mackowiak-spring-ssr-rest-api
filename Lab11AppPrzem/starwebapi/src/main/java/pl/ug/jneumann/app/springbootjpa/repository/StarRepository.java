package pl.ug.jneumann.app.springbootjpa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.ug.jneumann.app.springbootjpa.domain.Star.STARTYPE;
import pl.ug.jneumann.app.springbootjpa.domain.Star.Star;

import java.util.List;

@Repository
public interface StarRepository extends CrudRepository<Star, Long> {
    @Query("Select s from Star s where s.starType=?1")
    List<Star> findBySTARTYPE(STARTYPE starType);

    @Query("Select s from Star s where s.name=?1")
    List<Star> findByName(String name);

    @Query("Select s from Star s where s.name=?1 or s.starType=?2")
    List<Star> findByNameOrSTARTYPE(String name, STARTYPE starType);

    @Query("Select s from Star s where s.name=?1 or s.lifeStage=?2")
    List<Star> findByNameOrLifeStage(String name, String lifeStage);

    @Query("Select s from Star s where s.starType=?1 and s.lifeStage=?2")
    List<Star> findBySTARTYPEAndLifeStage(STARTYPE starType, String lifeStage);
}
