package pl.ug.jneumann.app.springbootjpa.domain;

import pl.ug.jneumann.app.springbootjpa.domain.Star.Star;

import javax.persistence.*;
import java.util.List;

@Entity
public class Universe {

  private Long id;
  private String name;
  private int cooledDownStars;
  private List<Star> burningStars;

  public Universe(String name, int cooledDownStars) {
    this.name = name;
    this.cooledDownStars = cooledDownStars;
  }

  public Universe() {
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  public List<Star> getBurningStars() {
    return burningStars;
  }

  public void setBurningStars(List<Star> stars) {
    this.burningStars = stars;
  }

  public int getCooledDownStars() {
    return cooledDownStars;
  }

  public void setCooledDownStars(int cooledDownStars) {
    this.cooledDownStars = cooledDownStars;
  }

  @Override
  public String toString() {
    return "Format{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", cooledDownStars=" + cooledDownStars  + '\'' +
        ", burningStars=" + burningStars + '\'' +
        '}';
  }
}
