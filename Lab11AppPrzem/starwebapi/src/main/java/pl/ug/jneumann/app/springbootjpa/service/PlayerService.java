package pl.ug.jneumann.app.springbootjpa.service;


import org.springframework.stereotype.Service;
import pl.ug.jneumann.app.springbootjpa.domain.Galaxy;
import pl.ug.jneumann.app.springbootjpa.domain.Constellation;
import pl.ug.jneumann.app.springbootjpa.repository.ConstellationRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ConstellationService {

  final ConstellationRepository constellationRepository;

  public ConstellationService(ConstellationRepository constellationRepository) {
    this.constellationRepository = constellationRepository;
  }

  public Constellation addConstellation(Constellation constellation) {
    return constellationRepository.save(constellation);
  }

  public Iterable<Constellation> findAllConstellations(){
    return constellationRepository.findAll();
  }

  public Iterable<Constellation> findConstellationByName(String name) {
    return constellationRepository.findByName(name);
  }

  public void learning() {
    // Tx.begin
    Constellation constellation1 = new Constellation("Andromeda", 55);
    Constellation constellation2 = new Constellation("Owl", 666);
    Constellation constellation3 = new Constellation("Monkey", 7777);
    Constellation constellation4 = new Constellation("Cassiopeia", 8888);

    List<Constellation> constellations = new ArrayList<>();
    constellations.add(constellation1);
    constellations.add(constellation2);
    constellations.add(constellation3);
    constellations.add(constellation4);

    constellationRepository.saveAll(constellations);

    System.out.println(constellationRepository.findByName("Andromeda"));

    // Tx.commit
    // Tx.rollback
  }
}
