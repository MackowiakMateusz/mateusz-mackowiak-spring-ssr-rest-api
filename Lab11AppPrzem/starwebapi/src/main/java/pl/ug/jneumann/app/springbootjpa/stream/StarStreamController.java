package pl.ug.jneumann.app.springbootjpa.stream;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.ug.jneumann.app.springbootjpa.domain.Star.STARTYPE;
import pl.ug.jneumann.app.springbootjpa.domain.Star.Star;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/starsstream")
public class StarStreamController {

    public StarStreamController() {}

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/flux", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Star> getAllFluxStars() {
        System.out.println("getAllFluxStars");
        Flux<Long> interval = Flux.interval(Duration.ofSeconds(1));
        Random random = new Random();
        List<String> words = Arrays.asList("Galxicus", "Maxicus", "Milky", "Way", "Void", "Maw", "Mocking", "Jaw", "Dimension", "X", "Eldritch", "Horror", "Nebula", "Group", "Zero", "777", "Hell", "Heaven", "Reaper", "Star", "Collection", "Shift", "Pass");
        List<String> rarities = Arrays.asList("Common", "Rare", "Ultra Rare");

        Flux<Star> events = interval
                .map((i) -> new Star(combineWords(words), randomEnumValue(STARTYPE.class), "Star Formation", randomWord(rarities), Math.abs(random.nextInt(100)) + 1));
        return events;
    }

    public static String combineWords(List<String> words) {
        Random rand = new Random();
        int index1 = rand.nextInt(words.size());
        int index2 = rand.nextInt(words.size());

        return words.get(index1) + " " + words.get(index2);
    }

    public static String randomWord(List<String> words) {
        Random rand = new Random();
        int index = rand.nextInt(words.size());
        return words.get(index);
    }

    public static <T extends Enum<STARTYPE>> T randomEnumValue(Class<T> enumValueClass){
        int x = enumValueClass.getEnumConstants().length;
        Random random = new Random();
        return enumValueClass.getEnumConstants()[random.nextInt(x)];
    }
}