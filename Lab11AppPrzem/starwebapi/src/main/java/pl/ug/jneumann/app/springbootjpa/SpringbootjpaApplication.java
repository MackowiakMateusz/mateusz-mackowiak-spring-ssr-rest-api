package pl.ug.jneumann.app.springbootjpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.ug.jneumann.app.springbootjpa.domain.Star.Star;
import pl.ug.jneumann.app.springbootjpa.service.StarService;
import pl.ug.jneumann.app.springbootjpa.service.UniverseService;
import pl.ug.jneumann.app.springbootjpa.service.GalaxyService;
import pl.ug.jneumann.app.springbootjpa.service.ConstellationService;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class SpringbootjpaApplication {
  Logger logger = LoggerFactory.getLogger(SpringbootjpaApplication.class);

  public static void main(String[] args) {
    SpringApplication.run(SpringbootjpaApplication.class, args);
  }

  @Bean
  public CommandLineRunner setUpApp(StarService starService, UniverseService universeService, GalaxyService galaxyService, ConstellationService constellationService) {
    return (args) -> {
      starService.learning();
      universeService.learning();
      constellationService.learning();
      galaxyService.learning();

      WebClient client = WebClient.create("http://localhost:8080");

      Flux<Star> employeeFlux = client.get()
              .uri("/starsstream/flux")
              .retrieve()
              .bodyToFlux(Star.class);

      employeeFlux.subscribe(System.out::println);
    };
  }
}
