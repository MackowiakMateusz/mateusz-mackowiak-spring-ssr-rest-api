package pl.ug.jneumann.app.springbootjpa.service;


import org.springframework.stereotype.Service;
import pl.ug.jneumann.app.springbootjpa.domain.Star.STARTYPE;
import pl.ug.jneumann.app.springbootjpa.domain.Star.Star;
import pl.ug.jneumann.app.springbootjpa.repository.StarRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class StarService {

  final StarRepository starRepository;

  public StarService(StarRepository starRepository) {
    this.starRepository = starRepository;
  }

  public Star addStar(Star star) {
    return starRepository.save(star);
  }

  public Iterable<Star> findAllStars() {
    return starRepository.findAll();
  }

  public Iterable<Star> findStarsByStarType(STARTYPE starType) {
    return starRepository.findByStarType(starType);
  }

  public Iterable<Star> findStarsByName(String name) {
    return starRepository.findByName(name);
  }

  public Iterable<Star> findStarsByStarTypeAndLifeStage(STARTYPE starType, String lifeStage) {
    return starRepository.findByStarTypeAndLifeStage(starType, lifeStage);
  }

  public Iterable<Star> findStarsByNameOrLifeStage(String name, String lifeStage) {
    return starRepository.findByNameOrLifeStage(name, lifeStage);
  }

  public Iterable<Star> findStarsByNameOrStarType(String name, STARTYPE starType) {
    return starRepository.findByNameOrStarType(name, starType);
  }

  public Optional<Star> getStarById(long id) {
    return starRepository.findById(id);
  }

  public void deleteStar(long id) {
    starRepository.deleteById(id);
  }

  public void learning() {
    Star star1 = new Star("EXAMPLE", STARTYPE.A, "Star Formation", "Common", 9);
    Star star2 = new Star("ALPHA CENTAURI", STARTYPE.B, "Childhood", "Rare", 111);
    Star star3 = new Star("SUNSHINE", STARTYPE.F, "Ignition", "Ultra Rare", 777);
    Star star4 = new Star("SOL", STARTYPE.G, "Cooling", "Common", 900000);

    List<Star> stars = new ArrayList<>();
    stars.add(star1);
    stars.add(star2);
    stars.add(star3);
    stars.add(star4);
    starRepository.saveAll(stars);

    System.out.println(starRepository.findByStarType(STARTYPE.F));
    System.out.println(starRepository.findByStarType(STARTYPE.B));
    System.out.println(starRepository.findByStarType(STARTYPE.G));
    System.out.println(starRepository.findByName("SOL"));
  }
}
