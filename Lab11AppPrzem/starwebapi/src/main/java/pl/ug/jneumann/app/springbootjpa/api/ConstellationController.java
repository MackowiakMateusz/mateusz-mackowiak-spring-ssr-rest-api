package pl.ug.jneumann.app.springbootjpa.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.ug.jneumann.app.springbootjpa.domain.Constellation;
import pl.ug.jneumann.app.springbootjpa.service.ConstellationService;

@RestController
public class ConstellationController {

  final
  ConstellationService constellationService;

  public ConstellationController(ConstellationService constellationService) {
    this.constellationService = constellationService;
  }

  @GetMapping("/api/constellations")
  Iterable<Constellation> allConstellations() {
    return constellationService.findAllConstellations();
  }

  @GetMapping("/api/constellations/byName/{name}")
  Iterable<Constellation> getGalaxyByName(@PathVariable String name) {
    return constellationService.findConstellationByName(name);
  }
}
