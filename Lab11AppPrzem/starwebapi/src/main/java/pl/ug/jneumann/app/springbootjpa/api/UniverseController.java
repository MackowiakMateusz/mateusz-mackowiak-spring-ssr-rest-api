package pl.ug.jneumann.app.springbootjpa.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.ug.jneumann.app.springbootjpa.domain.Universe;
import pl.ug.jneumann.app.springbootjpa.service.UniverseService;

@RestController
public class UniverseController {

  final
  UniverseService universeService;

  public UniverseController(UniverseService universeService) {
    this.universeService = universeService;
  }

  @GetMapping("/api/universes")
  Iterable<Universe> allUniverses() {
    return universeService.findAllUniverses();
  }

  @GetMapping("/api/universes/byName/{name}")
  Iterable<Universe> getUniverseByName(@PathVariable String name) {
    return universeService.findByName(name);
  }
}
