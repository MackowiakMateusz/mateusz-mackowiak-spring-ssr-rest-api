package pl.ug.jneumann.app.springbootjpa.service;


import org.springframework.stereotype.Service;
import pl.ug.jneumann.app.springbootjpa.domain.Galaxy;
import pl.ug.jneumann.app.springbootjpa.repository.GalaxyRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class GalaxyService {

  final GalaxyRepository galaxyRepository;

  public GalaxyService(GalaxyRepository galaxyRepository) {
    this.galaxyRepository = galaxyRepository;
  }

  public Galaxy addGalaxy(Galaxy galaxy) {
    return galaxyRepository.save(galaxy);
  }

  public Iterable<Galaxy> findAllGalaxies() {
    return galaxyRepository.findAll();
  }

  public Iterable<Galaxy> findGalaxyByName(String name) {
    return galaxyRepository.findByName(name);
  }

  public void learning() {
    Galaxy galaxy = new Galaxy("MilkyWay");

    List<Galaxy> galaxies = new ArrayList<>();
    galaxies.add(galaxy);

    galaxyRepository.saveAll(galaxies);

    System.out.println(galaxyRepository.findByName("MilkyWay"));
  }
}
