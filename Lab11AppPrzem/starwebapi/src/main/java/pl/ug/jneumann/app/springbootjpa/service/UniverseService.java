package pl.ug.jneumann.app.springbootjpa.service;


import org.springframework.stereotype.Service;
import pl.ug.jneumann.app.springbootjpa.domain.Universe;
import pl.ug.jneumann.app.springbootjpa.repository.UniverseRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UniverseService {

  final UniverseRepository universeRepository;

  public UniverseService(UniverseRepository universeRepository) {
    this.universeRepository = universeRepository;
  }

  public Universe addUniverse(Universe universe) {
    return universeRepository.save(universe);
  }

  public Iterable<Universe> findAllUniverses() {
    return universeRepository.findAll();
  }

  public Iterable<Universe> findByName(String name) {
    return universeRepository.findByName(name);
  }

  public void learning() {

    Universe universe1 = new Universe("Dragonball", 777);
    Universe universe2 = new Universe("Real", 77);


    List<Universe> universes = new ArrayList<>();
    universes.add(universe1);
    universes.add(universe2);

    universeRepository.saveAll(universes);


    System.out.println(universeRepository.findByName("Dragonball"));

  }
}
