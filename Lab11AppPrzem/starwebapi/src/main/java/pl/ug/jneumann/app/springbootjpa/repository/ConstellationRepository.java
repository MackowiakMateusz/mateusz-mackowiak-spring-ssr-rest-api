package pl.ug.jneumann.app.springbootjpa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.ug.jneumann.app.springbootjpa.domain.Constellation;

import java.util.List;

@Repository
public interface ConstellationRepository extends CrudRepository<Constellation, Long> {
    @Query("Select c from Constellation c where c.name=?1")
    List<Constellation> findByName(String name);
}
