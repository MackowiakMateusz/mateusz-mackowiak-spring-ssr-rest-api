package pl.ug.jneumann.app.springbootjpa.domain;

import org.springframework.lang.NonNull;
import pl.ug.jneumann.app.springbootjpa.domain.Star.Star;

import javax.persistence.*;


@Entity
public class Galaxy {

  private Long id;
  private String nickname;
  private int constellationsOwned;
  private Star givesFreeStar;

  public Galaxy() {
  }

  public Galaxy(String nickname) {
    this.nickname = nickname;
  }

  @OneToOne
  public Star getGivesFreeStar() {
    return givesFreeStar;
  }

  public void setGivesFreeStar(Star givesFreeStar) {
    this.givesFreeStar = givesFreeStar;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @NonNull
  public String getNickname() {
    return nickname;
  }

  public void setNickname(String firstName) {
    this.nickname = firstName;
  }

  @Column(name = "constellations_owned")
  public int getTournamentsHeld() {
    return constellationsOwned;
  }

  public void setTournamentsHeld(int constellationsOwned) {
    this.constellationsOwned = constellationsOwned;
  }
}
