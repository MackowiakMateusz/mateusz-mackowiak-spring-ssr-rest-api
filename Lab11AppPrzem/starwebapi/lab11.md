**Zadanie 1**

Na podstawie przykładu z wykładu:

1. zapoznaj się z API Java Streams - czyli klasycznych, nieraktywnych sturmieni, 
 stwórz własny strumień i wykonuj różne operacje jak w przykładzie [Java Streams](https://stackify.com/streams-guide-java-8/)

2. we własnej dziedzinie, utwórz strumień reaktywny, potencjalnie nieskończonych zdarzeń 
 (por. klasę `TemperatureEvent` i `TemperatureService`) wraz z `RequestHandler,` i `RequestRouter`. 
 Po uruchomieniu sprawdź działanie za pomocą `curl -v localhost:8080/tempstream`. Zaobserwuj jakie nagłówki HTTP występują w strumieniu reaktywnym.

3. dla webowego, reaktywnego strumienia z pkt. 2. napisz reaktywnego klienta webowego (w osobnym projekcie)
i wykonuj różne operacje na strumieniu reaktywnym, stwórz własnego subskrybenta. Przykładowe operacje na strumieniach: 
 filtrowanie - wybieranie zdarzeń spełniających określone kryteria, zliczanie ilości/liczby w jednostce czasu (np. średnia na minutę), mapowanie - zamiana typu zdarzeń na inne


**Przydatne materiały**

* [Operatory/operacje reaktywne](https://reactivex.io/documentation/operators.html)
* [Interaktywne diagramy Marbla](https://rxmarbles.com/)
* [Turorial o Spring WebFlux](https://www.cognizantsoftvision.com/blog/getting-started-with-reactive-spring-spring-webflux/)



