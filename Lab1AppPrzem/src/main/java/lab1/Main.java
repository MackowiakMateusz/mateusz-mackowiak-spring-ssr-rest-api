package lab1;

import lab1.exercise5.Gender;

public class Main {
    public static void main(String[] args) throws java.io.IOException {
        System.out.println("Test/s for exercise 1 ->");
        Exercise1.showMessage();
        System.out.println("Test/s for exercise 2 ->");
        System.out.println(Exercise2.isArmstrong(371));
        System.out.println(Exercise2.isArmstrong(370));
        System.out.println("Test/s for exercise 3 ->");
        System.out.println(Exercise3.ageInYears(1000000000, Exercise3.Planet.EARTH));
        System.out.println(Exercise3.ageInYears(1000000000, Exercise3.Planet.MERCURY));
        System.out.println(Exercise3.ageInYears(1000000000, Exercise3.Planet.VENUS));
        System.out.println(Exercise3.ageInYears(1000000000, Exercise3.Planet.MARS));
        System.out.println(Exercise3.ageInYears(1000000000, Exercise3.Planet.JUPITER));
        System.out.println(Exercise3.ageInYears(1000000000, Exercise3.Planet.SATURN));
        System.out.println(Exercise3.ageInYears(1000000000, Exercise3.Planet.URANUS));
        System.out.println(Exercise3.ageInYears(1000000000, Exercise3.Planet.NEPTUNE));
        System.out.println("Test/s for exercise 4 ->");
        Exercise4.printStringBasedOnN(3);
        System.out.println("Test/s for exercise 5 Author ->");
        lab1.exercise5.Author author = new lab1.exercise5.Author("bob","bob@gmail.com", Gender.Male);
        System.out.println(author.getName()=="bob");
        System.out.println(author.getEmail()=="bob@gmail.com");
        author.setEmail("bob123@gmail.com");
        System.out.println(author.getEmail()=="bob123@gmail.com");
        System.out.println(author.getGender()==Gender.Male);
        System.out.println(author.toString().contains("Author[name=bob,email=bob123@gmail.com,gender=Male]"));
        System.out.println("Test/s for exercise 5 Book ->");
        lab1.exercise5.Book book = new lab1.exercise5.Book("ArtOfWar",author,1.0);

    }
}
