package lab1;

public class Exercise4 {
    public static void printStringBasedOnN(int n) {
        for (int i=0;i<=n;i++)
        {
            if(i%2==0)
            {
                System.out.println("x\n" +
                        "xx\n" +
                        "xxx");
            }
            else
            {
                System.out.println("xxx\n" +
                        "xx\n" +
                        "x");
            }
        }
    }
}
