package lab1.exercise5;

public class Author {

    String name;
    String email;
    Gender gender;

    public Author(String name, String email,Gender gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }
    public String getName()
    {
        return name;
    }
    public String getEmail()
    {
        return email;
    }
    public Gender getGender()
    {
        return gender;
    }
    public void setEmail(String email)
    {
        this.email=email;
    }

    @Override
    public String toString() {
        return "Author[name="+name+",email="+email+",gender="+gender+"]";
    }
}
