package lab1.exercise5;

public class Book {
    String name;
    Author author;
    double price;
    int qty=0;
    public Book(String name, Author author,double price,int qty) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qty = qty;
    }
    public Book(String name, Author author,double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }
    public String getName()
    {
        return name;
    }
    public Author getAuthor()
    {
        return author;
    }
    public double getPrice()
    {
        return price;
    }
    public int getQty()
    {
        return qty;
    }
    public int setQty(double qty)
    {
        return this.qty;
    }
    public void setPrice(double price)
    {
        price=this.price;
    }

    @Override
    public String toString() {
        return "Book[name="+name+",author="+author.toString()+",price="+price+",qty="+qty+"]";
    }
}
