package lab1.exercise5;

public enum Gender {
    Male,
    Female,
    Other;
}
