package lab1;

public class Exercise3
{
    public enum Planet {
        MERCURY (0.2408467 ),
        VENUS   (0.61519726),
        EARTH   (1.0       ),
        MARS    (1.8808158 ),
        JUPITER (11.862615 ),
        SATURN  (29.447498 ),
        URANUS  (84.016846 ),
        NEPTUNE (164.79132 );

        private final double orbitalPeriod;   // in seconds
        Planet(double orbitalPeriod) {
            this.orbitalPeriod = orbitalPeriod;
        }
    }

    public static double ageInYears(int seconds,Planet planet) {
        return seconds*planet.orbitalPeriod/31557600;
    }
}
